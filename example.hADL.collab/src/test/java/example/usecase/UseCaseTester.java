package example.usecase;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.hipchat.THipChatRoomDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.platform.google.TGDriveCollabPlatform;
import at.ac.tuwien.dsg.hadl.schema.extension.platform.hipchat.THipChatCollabPlatform;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import at.ac.tuwien.dsg.hadl.surrogates.gdrive.GoogleUtils;
import at.ac.tuwien.dsg.hadl.surrogates.hipchat.HipChatUtils;

public class UseCaseTester 
{
	protected UseCaseUtils ucu = new UseCaseUtils();		
	// PROVIDE YOUR OWN IMPLEMENTATION OF IUseCaseConfig that returns YOUR google=hipchat users
	private IUseCaseConfig ucc = new UseCaseConfig();
	
	@Before
	public void setUp() throws Exception {
		TGDriveCollabPlatform cred = new TGDriveCollabPlatform();
		cred.setCredentialsFile(ucc.getCredentialsFileLocation());
		cred.setServiceAccount(ucc.getServiceAccountEmail());
		THipChatCollabPlatform hipChatConfig = new THipChatCollabPlatform();
		hipChatConfig.setServiceAccount(ucc.getHipChatServiceAccount());
		hipChatConfig.setToken(ucc.getHipChatToken());
		ucu.setupRuntimeEnvironment(cred, hipChatConfig);					
	}
	
	@Test
	public void runChatPartOnly() throws Exception
	{
		ucu.app.init(ucu.scope);
		
		TGoogleUserDescriptor gud1 = ucc.getUser1();
		TGoogleUserDescriptor gud2 = ucc.getUser2();
		ucu.acquireChatUser(gud1);
		ucu.acquireChatUser(gud2);
		THipChatRoomDescriptor criteriaRoom = HipChatUtils.getNewRoomDescriptor("Committee Discussion Room", "Discussion on all Applications");
		ucu.acquireRoom(criteriaRoom);
		ucu.linkChatUserToRoom(gud1, criteriaRoom);
		ucu.linkChatUserToRoom(gud2, criteriaRoom);
		ucu.start();
		// -------------------------------------------------------
		ucu.stop();
		// Setup chatroom for assessment team + department members for discussion
		THipChatRoomDescriptor assessmentRoom = HipChatUtils.getNewRoomDescriptor("Assessment 1 Discussion Room", "Discussion on Application 1");
		ucu.acquireRoom(assessmentRoom);
		TGoogleUserDescriptor gud3 = ucc.getUser3();
		TGoogleUserDescriptor gud4 = ucc.getUser4();
		ucu.acquireChatUser(gud3);
		ucu.acquireChatUser(gud4);
		ucu.linkChatUserToRoom(gud1, assessmentRoom);
		ucu.linkChatUserToRoom(gud2, assessmentRoom);
		ucu.linkChatUserToRoom(gud3, assessmentRoom);
		ucu.linkChatUserToRoom(gud4, assessmentRoom);
		ucu.start();
		// ------------------------------------------------------
		ucu.stop();
		ucu.releaseRoom(assessmentRoom);
		ucu.start();
		// ------------------------------------------------------
		ucu.stop();
		ucu.releaseAll();			
		ucu.shutdownRuntimeEnvironment();
	}
	
		
	@Test
	public void runUseCase() throws Exception
	{		
		ucu.app.init(ucu.scope);
		
		// PHASE 1
		// hiring process preparation steps
		// preparation for step <Evaluation Criteria Meeting>
		// process engine or participants create "CriteriaDocument", then adding of editors		
		TGoogleFileDescriptor criteriaDoc = GoogleUtils.getNewFileDescriptor("Evaluation Criteria Document");
		ucu.acquireFile(criteriaDoc);
		TGoogleUserDescriptor gud1 = ucc.getUser1();
		ucu.acquireUser(gud1);	
		TGoogleUserDescriptor gud2 = ucc.getUser2();
		ucu.acquireUser(gud2);
		ucu.linkEditingUserToFile(gud1, criteriaDoc);
		ucu.linkEditingUserToFile(gud2, criteriaDoc);
		ucu.start();
		// -----------------------------------------------------------------------------------------
		// meeting takes place
		// 
		// after meeting: process sets editors to commenters and setup chat room
		ucu.stop();
		ucu.unlinkUserFromFile(gud1, criteriaDoc);
		ucu.unlinkUserFromFile(gud2, criteriaDoc);
		ucu.linkCommentingUserToFile(gud1, criteriaDoc);
		ucu.linkCommentingUserToFile(gud2, criteriaDoc);
		
		ucu.acquireChatUser(gud1);
		ucu.acquireChatUser(gud2);
		THipChatRoomDescriptor committeeRoom = HipChatUtils.getNewRoomDescriptor("Committee Discussion Room", "Discussion on all Applications");
		ucu.acquireRoom(committeeRoom);
		ucu.linkChatUserToRoom(gud1, committeeRoom);
		ucu.linkChatUserToRoom(gud2, committeeRoom);
		ucu.start();
		// -----------------------------------------------------------------------------------------
		
		// PHASE 2
		// assessment loop - here exemplary 1 job application
		ucu.stop();
		// process engine or participants provides "Job Application Document", then adding of readers				
		TGoogleFileDescriptor appl1Doc = GoogleUtils.getNewFileDescriptor("Job Application 1 Doc");
		ucu.acquireFile(appl1Doc);
		// set the assessment team members
		// reuse User 1 of the hiring commitee, and now also add second new user 3
		ucu.linkReadingUserToFile(gud1, appl1Doc);
		TGoogleUserDescriptor gud3 = ucc.getUser3();
		ucu.acquireUser(gud3);
		ucu.linkReadingUserToFile(gud3, appl1Doc);
		// create assessment document and add team members as editors, all others as commenters
		TGoogleFileDescriptor assess1Doc = GoogleUtils.getNewFileDescriptor("Assessment 1 Doc");
		ucu.acquireFile(assess1Doc);
		ucu.linkEditingUserToFile(gud1, assess1Doc);
		ucu.linkEditingUserToFile(gud3, assess1Doc);
		ucu.linkCommentingUserToFile(gud2, assess1Doc);
		TGoogleUserDescriptor gud4 = ucc.getUser4();
		ucu.acquireUser(gud4);
		ucu.linkCommentingUserToFile(gud4, assess1Doc);					
		//add minority officer with comment rights
		TGoogleUserDescriptor gud5 = ucc.getUser5();
		ucu.acquireUser(gud5);
		ucu.linkCommentingUserToFile(gud5, assess1Doc);
				
		// Setup chatroom for assessment team + department members for discussion
		ucu.acquireChatUser(gud3);
		ucu.acquireChatUser(gud4);
		THipChatRoomDescriptor assessmentRoom = HipChatUtils.getNewRoomDescriptor("Assessment 1 Discussion Room", "Discussion on Application 1");
		ucu.acquireRoom(assessmentRoom);
		ucu.linkChatUserToRoom(gud1, assessmentRoom);
		ucu.linkChatUserToRoom(gud2, assessmentRoom);
		ucu.linkChatUserToRoom(gud3, assessmentRoom);
		ucu.linkChatUserToRoom(gud4, assessmentRoom);
		
		ucu.start();
		// -----------------------------------------------------------------------------------------
		// assessment and commenting takes place
		//
		// upon assessment deadline set all users to readers of assessment and remove them from application
		ucu.stop();
		ucu.unlinkUserFromFile(gud1, appl1Doc);
		ucu.unlinkUserFromFile(gud3, appl1Doc);		
		ucu.unlinkUserFromFile(gud1, assess1Doc);
		ucu.unlinkUserFromFile(gud3, assess1Doc);
		ucu.unlinkUserFromFile(gud3, assess1Doc);
		ucu.unlinkUserFromFile(gud4, assess1Doc);		
		ucu.linkReadingUserToFile(gud1, assess1Doc);
		ucu.linkReadingUserToFile(gud2, assess1Doc);
		ucu.linkReadingUserToFile(gud3, assess1Doc);
		ucu.linkReadingUserToFile(gud4, assess1Doc);			
		// keep minority officer with commenting access - no op
		//
		// shut-down discussion chat room
		ucu.releaseRoom(assessmentRoom);
		
		ucu.start();
		// -----------------------------------------------------------------------------------------
		// PHASE 3
		// candidate selection steps
		// Hiring committee members already have access to doc, 
		// thus only create list of candidates
		ucu.stop();
		TGoogleFileDescriptor listDoc = GoogleUtils.getNewFileDescriptor("Candidate List 1 Doc");
		ucu.acquireFile(listDoc);
		ucu.linkEditingUserToFile(gud1, listDoc);
		ucu.linkEditingUserToFile(gud2, listDoc);
		// give minority officer comment access
		ucu.linkCommentingUserToFile(gud5, listDoc);
		ucu.start();
		// -----------------------------------------------------------------------------------------
		// have the meeting
		//
		// after the meeting
		ucu.stop();
		ucu.unlinkUserFromFile(gud1, listDoc);
		ucu.unlinkUserFromFile(gud2, listDoc);
		ucu.linkReadingUserToFile(gud1, listDoc);
		ucu.linkReadingUserToFile(gud2, listDoc);
		// remove minority officer from assessment report(s)
		ucu.unlinkUserFromFile(gud5, assess1Doc);
		ucu.start();		
		// -----------------------------------------------------------------------------------------
		// after minority officer confirms (here via assumed process only) do
		// clean up
		ucu.stop();
		ucu.releaseAll();			
		ucu.shutdownRuntimeEnvironment();
	}
	
	
		
}
