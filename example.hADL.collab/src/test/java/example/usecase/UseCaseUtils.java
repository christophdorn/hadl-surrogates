package example.usecase;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;

import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import rx.functions.Action0;
import rx.functions.Action1;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.Executable2OperationalTransformer;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLLinkageConnector;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeModel;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeMonitor;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.Neo4JbackedRuntimeModel;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.RuntimeRegistry;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.SurrogateFactoryResolver;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ESurrogateStatus;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.FactoryResolvingException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.MultiFactoryException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import at.ac.tuwien.dsg.hadl.schema.core.ObjectFactory;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabLink;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement.Extension;
import at.ac.tuwien.dsg.hadl.schema.executable.TCollabPlatform;
import at.ac.tuwien.dsg.hadl.schema.executable.TExecutables;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.hipchat.THipChatRoomDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.platform.google.TGDriveCollabPlatform;
import at.ac.tuwien.dsg.hadl.schema.extension.platform.hipchat.THipChatCollabPlatform;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import at.ac.tuwien.dsg.hadl.surrogates.Collab;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;

public class UseCaseUtils {

	private static String MODEL_FILE = "./src/main/resources/usecase-hADL.xml";
	private static String OUTPUT_PATH_PREFIX = "target/output/gdrive/";
	
	protected static final Logger logger = LogManager.getLogger(UseCaseUtils.class);
	
	protected Injector injector;
	protected HADLLinkageConnector app;
	protected TActivityScope scope;	
	protected ModelTypesUtil mtu;
	protected HADLmodel model;
	protected Neo4JbackedRuntimeModel hrm;
	protected SurrogateFactoryResolver sfr;
	
	public void shutdownRuntimeEnvironment()
	{
		app.shutdown();			
		injector = null;
		scope = null;		
		app = null;
	}
	
	public void setupRuntimeEnvironment(TGDriveCollabPlatform credentialsConfig, THipChatCollabPlatform hipChatConfig) throws Exception
	{

		model = loadModel();
		mtu = new ModelTypesUtil();
		mtu.init(model);
		setGoogleCredentials(model, credentialsConfig);		
		setHipChatCredentials(model, hipChatConfig);
		scope = mtu.retrieveScopeById(Collab.SCOPE_fullScope);
		// TODO merge in ChatScope but not essential
		assertNotNull(scope);

		final Injector baseInjector = Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {				
			}

			@Provides
			@Singleton
			Executable2OperationalTransformer getTransformer()
			{				
				return new Executable2OperationalTransformer();
			}

			@Provides 
			@Singleton
			ModelTypesUtil getTypesUtil()
			{				
				return mtu;
			}					
		});

		injector = Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {
				bind(HADLLinkageConnector.class).in(Singleton.class);				
			}			

			@Provides
			@Singleton
			Executable2OperationalTransformer getTransformer()
			{				
				return baseInjector.getInstance(Executable2OperationalTransformer.class);
			}

			@Provides
			@Singleton
			RuntimeRegistry getRegistry()
			{
				return new RuntimeRegistry();				
			}

			@Provides 
			@Singleton
			ModelTypesUtil getTypesUtil()
			{
				return baseInjector.getInstance(ModelTypesUtil.class);
			}

			@Provides
			@Singleton
			SurrogateFactoryResolver getFactoryResolver()
			{
				sfr = new SurrogateFactoryResolver(baseInjector);				
				return sfr; 				
			}

			@Provides
			@Singleton
			HADLruntimeModel getRuntimeModel()
			{
				hrm = new Neo4JbackedRuntimeModel();
				baseInjector.injectMembers(hrm);				
				return hrm;  							
			}

		});			
		app = injector.getInstance(HADLLinkageConnector.class);		
		String DB_PATH = "target/neo4j-store";
		org.apache.commons.io.FileUtils.deleteQuietly(new File(DB_PATH));
		GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( DB_PATH ); 								
		hrm.init(graphDb, model);	
		sfr.resolveFactoriesFrom(model);
	}
	
	
	private void setGoogleCredentials(HADLmodel model, TGDriveCollabPlatform credentialsConfig) throws MultiFactoryException
	{ 	
		for (Extension ext : model.getExtension())
		{
			Object obj = ext.getAny();

			if (obj instanceof JAXBElement)
				obj = ((JAXBElement)obj).getValue();
			if (obj instanceof TExecutables)
			{
				TCollabPlatform platform = ((TExecutables) obj).getCollabPlatform();
				if (platform instanceof TGDriveCollabPlatform)
				{
					TGDriveCollabPlatform gdrive = (TGDriveCollabPlatform)platform;
					gdrive.setCredentialsFile(credentialsConfig.getCredentialsFile());
					gdrive.setServiceAccount(credentialsConfig.getServiceAccount());
					return;
				}
			}
		}			
		fail("No TGDriveCollabPlatform found");
	} 
	
	private void setHipChatCredentials(HADLmodel model, THipChatCollabPlatform credentialsConfig) throws MultiFactoryException
	{
		for (Extension ext : model.getExtension())
		{
			Object obj = ext.getAny();

			if (obj instanceof JAXBElement)
				obj = ((JAXBElement)obj).getValue();
			if (obj instanceof TExecutables)
			{
				TCollabPlatform platform = ((TExecutables) obj).getCollabPlatform();
				if (platform instanceof THipChatCollabPlatform)
				{
					THipChatCollabPlatform hipchat = (THipChatCollabPlatform)platform;
					hipchat.setToken(credentialsConfig.getToken());
					hipchat.setServiceAccount(credentialsConfig.getServiceAccount());
					return;
				}
			}
		}			
		fail("No THipChatCollabPlatform found");
		
	}
	
	private HADLmodel loadModel()
	{
		try {
			JAXBContext jc = JAXBContext.newInstance(ObjectFactory.class, 
					at.ac.tuwien.dsg.hadl.schema.executable.ObjectFactory.class,
					at.ac.tuwien.dsg.hadl.schema.extension.platform.google.ObjectFactory.class,
					at.ac.tuwien.dsg.hadl.schema.extension.platform.hipchat.ObjectFactory.class); 
			Unmarshaller unmarshaller = jc.createUnmarshaller();			
			File xml = new File(MODEL_FILE);
			model = (HADLmodel) unmarshaller.unmarshal(xml);
			File of = new File(OUTPUT_PATH_PREFIX);
			of.mkdirs();
			org.apache.commons.io.FileUtils.cleanDirectory(of);			
			
		} catch (Exception e) {			
			e.printStackTrace();
			model = null;
			fail();		
		}			
		return model;
	}

	
	private Hashtable<String, Set<TResourceDescriptor>> rdByType = new Hashtable<String, Set<TResourceDescriptor>>();
	private Hashtable<Map.Entry<String, TResourceDescriptor>, THADLarchElement> opEls = new Hashtable<Map.Entry<String,TResourceDescriptor>, THADLarchElement>();  
	private Hashtable<Map.Entry<TResourceDescriptor, TResourceDescriptor>, TOperationalCollabLink> opLinks = new Hashtable<Map.Entry<TResourceDescriptor,TResourceDescriptor>, TOperationalCollabLink>();
	
	protected void addAcquiredElement(THADLarchElement type, TResourceDescriptor rd, THADLarchElement opEl)
	{
		Set<TResourceDescriptor> set = null;
		if (rdByType.containsKey(type.getId()))
		{
			set = rdByType.get(type.getId());
		}
		else
		{
			set = new HashSet<TResourceDescriptor>();
			rdByType.put(type.getId(), set);
		}
		set.add(rd);
		
		if (opEl != null && !opEls.containsKey(new AbstractMap.SimpleEntry<String, TResourceDescriptor>(type.getId(), rd)))
		{
			opEls.put(new AbstractMap.SimpleEntry<String, TResourceDescriptor>(type.getId(), rd), opEl);
		}
	}
	
	protected Hashtable<String, Set<TResourceDescriptor>> getResourceDescriptorsOfOperationalElements()
	{
		return rdByType;
	}
	
	protected THADLarchElement getOperationalElementForResourceDescriptorAndType(THADLarchElement type, TResourceDescriptor rd)
	{
		return opEls.get(new AbstractMap.SimpleEntry<String, TResourceDescriptor>(type.getId(), rd));
	}
	
	protected void addOpLink(TResourceDescriptor collabRD, TResourceDescriptor objRD, TOperationalCollabLink link)
	{
		opLinks.put(new AbstractMap.SimpleEntry<TResourceDescriptor, TResourceDescriptor>(collabRD, objRD), link);
	}
	
	protected void acquireFile(TGoogleFileDescriptor rd) throws Exception
	{
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<Map.Entry<THADLarchElement,TResourceDescriptor>>();
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(mtu.getById(Collab.ART_File), rd));		
		final CountDownLatch lock = new CountDownLatch(1);
		app.acquireResourcesAsElements(mapping).subscribe(onAction(), onError(), onCompleted(lock, "Acquire"));
		lock.await();
	}
	
	protected void acquireUser(TGoogleUserDescriptor rd) throws Exception
	{
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<Map.Entry<THADLarchElement,TResourceDescriptor>>();
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(mtu.getById(Collab.COMP_User), rd));		
		final CountDownLatch lock = new CountDownLatch(1);
		app.acquireResourcesAsElements(mapping).subscribe(onAction(), onError(), onCompleted(lock, "Acquire"));
		lock.await();
	}
	
	protected void acquireChatUser(TGoogleUserDescriptor rd) throws Exception
	{
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<Map.Entry<THADLarchElement,TResourceDescriptor>>();
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(mtu.getById(Collab.COMP_ChatUser), rd));		
		final CountDownLatch lock = new CountDownLatch(1);
		app.acquireResourcesAsElements(mapping).subscribe(onAction(), onError(), onCompleted(lock, "Acquire"));
		lock.await();
	}
	
	protected void acquireRoom(THipChatRoomDescriptor rd) throws Exception
	{
		List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<Map.Entry<THADLarchElement,TResourceDescriptor>>();
		mapping.add(new AbstractMap.SimpleEntry<THADLarchElement, TResourceDescriptor>(mtu.getById(Collab.STM_ChatRoom), rd));		
		final CountDownLatch lock = new CountDownLatch(1);
		app.acquireResourcesAsElements(mapping).subscribe(onAction(), onError(), onCompleted(lock, "Acquire"));
		lock.await();
	}
	
	protected void linkChatUserToRoom(TGoogleUserDescriptor gud, THipChatRoomDescriptor gfd) throws Exception
	{	
		final CountDownLatch lock = new CountDownLatch(1);
		app.wire((TOperationalComponent)getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.COMP_ChatUser), gud),
				(TOperationalObject) getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.STM_ChatRoom), gfd),
				(TCollabLink)mtu.getById(Collab.LINK_chatting)).subscribe(onAction(), onError(), onCompleted(lock, "WireUp"));
		lock.await();
	}
	
	protected void unlinkChatUserFromRoom(TGoogleUserDescriptor gud, THipChatRoomDescriptor gfd) throws Exception
	{	
		final CountDownLatch lock = new CountDownLatch(1);
		app.unwire(opLinks.get(new AbstractMap.SimpleEntry<>(gud, gfd)))				
				.subscribe(onAction(), onError(), onCompleted(lock, "WireUp"));
		lock.await();
	}	
	
	protected void releaseRoom(THipChatRoomDescriptor rd) throws Exception
	{
		final CountDownLatch lock = new CountDownLatch(1);
		app.release((TOperationalObject) getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.STM_ChatRoom), rd))				
			.subscribe(onAction(), onError(), onCompleted(lock, "Release"));
		lock.await();
	}
	
	protected void releaseAll() throws Exception
	{			
		final CountDownLatch lock = new CountDownLatch(1);
		app.releaseAllResources().subscribe(onAction(), onError(), onCompleted(lock, "Release All"));
		lock.await();
	}
	
	protected void linkReadingUserToFile(TGoogleUserDescriptor gud, TGoogleFileDescriptor gfd) throws Exception
	{	
		final CountDownLatch lock = new CountDownLatch(1);
		app.wire((TOperationalComponent)getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.COMP_User), gud),
				(TOperationalObject) getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.ART_File), gfd),
				(TCollabLink)mtu.getById(Collab.LINK_reading)).subscribe(onAction(), onError(), onCompleted(lock, "WireUp"));
		lock.await();
	}
	
	protected void linkCommentingUserToFile(TGoogleUserDescriptor gud, TGoogleFileDescriptor gfd) throws Exception
	{	
		final CountDownLatch lock = new CountDownLatch(1);
		app.wire((TOperationalComponent)getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.COMP_User), gud),
				(TOperationalObject) getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.ART_File), gfd),
				(TCollabLink)mtu.getById(Collab.LINK_commenting)).subscribe(onAction(), onError(), onCompleted(lock, "WireUp"));
		lock.await();
	}
	
	protected void linkEditingUserToFile(TGoogleUserDescriptor gud, TGoogleFileDescriptor gfd) throws Exception
	{	
		final CountDownLatch lock = new CountDownLatch(1);
		app.wire((TOperationalComponent)getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.COMP_User), gud),
				(TOperationalObject) getOperationalElementForResourceDescriptorAndType(mtu.getById(Collab.ART_File), gfd),
				(TCollabLink)mtu.getById(Collab.LINK_editing)).subscribe(onAction(), onError(), onCompleted(lock, "WireUp"));
		lock.await();
	}
	
	protected void unlinkUserFromFile(TGoogleUserDescriptor gud, TGoogleFileDescriptor gfd) throws Exception
	{	
		final CountDownLatch lock = new CountDownLatch(1);
		app.unwire(opLinks.get(new AbstractMap.SimpleEntry<>(gud, gfd))).
			subscribe(onAction(), onError(), onCompleted(lock, "Unwire"));
		lock.await();
	}
	
	protected void start() throws Exception
	{
		final CountDownLatch lock = new CountDownLatch(1);
		app.startScope().subscribe(onAction(), onError(), onCompleted(lock, "Start"));
		lock.await();
	}
	
	protected void stop() throws Exception
	{
		final CountDownLatch lock = new CountDownLatch(1);
		app.stopScope().subscribe(onAction(), onError(), onCompleted(lock, "Stop"));
		lock.await();
	}
	
	protected Action1<SurrogateEvent> onAction()
	{
		return new Action1<SurrogateEvent>() {
			@Override
			public void call(SurrogateEvent t) {				
				
				if (t.getStatus().equals(ESurrogateStatus.ACQUIRING_SUCCESS))
				{					
					THADLarchElement el = t.getOperationalElement();
					if (el instanceof TOperationalComponent)
					{
						THADLarchElement type = mtu.getByTypeRef(((TOperationalComponent) el).getInstanceOf());
						addAcquiredElement(type, ((TOperationalComponent) el).getResourceDescriptor().get(0), el);
					}
					if (el instanceof TOperationalConnector)
					{
						THADLarchElement type = mtu.getByTypeRef(((TOperationalConnector) el).getInstanceOf());
						addAcquiredElement(type, ((TOperationalConnector) el).getResourceDescriptor().get(0), el);
					}
					if (el instanceof TOperationalObject)
					{
						THADLarchElement type = mtu.getByTypeRef(((TOperationalObject) el).getInstanceOf());
						addAcquiredElement(type, ((TOperationalObject) el).getResourceDescriptor().get(0), el);
					}
					logger.trace(t.getStatus() +" : "+t.getSource().getHref());
				}
				else if(t.getStatus().equals(ESurrogateStatus.REWIRING_SUCCESS))
				{
					THADLarchElement el = t.getOperationalElement();
					if (el instanceof TOperationalCollabLink)						
					{
						Triple<TOperationalObject, TOperationalComponent, TOperationalConnector> endpoints = hrm.getOperationalPairForLink((TOperationalCollabLink) el);
						TResourceDescriptor objRD = endpoints.getLeft().getResourceDescriptor().get(0);
						TResourceDescriptor collabRD = endpoints.getMiddle().getResourceDescriptor().get(0);
						addOpLink(collabRD, objRD, (TOperationalCollabLink) el);
					}
					logger.trace(t.getStatus() +" : "+t.getSource().getHref());
				}
				else if (t.getStatus().toString().endsWith("_FAILED") && t.getOptionalEx() != null)
				{
					logger.warn(t.getStatus() +" : "+t.getSource().getHref(), t.getOptionalEx());
				}
				else
					logger.trace(t.getStatus() +" : "+t.getSource().getHref());
			}
		};
	}
	
	protected static Action1<Throwable> onError()
	{
		return new Action1<Throwable>() {
			@Override
			public void call(Throwable t1) {
				logger.error("encountered error: ", t1);				
			}
		};
	}
	
	protected static Action0 onCompleted(final CountDownLatch lock, final String logPrefix)
	{
		return new Action0() {
			@Override
			public void call() {								
				logger.trace((logPrefix != null ? logPrefix : "")+" Observer Completed");
				if (lock != null)
					lock.countDown();
			}
		};
	}
}
