package example.usecase;

import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;

public interface IUseCaseConfig {

	public TGoogleUserDescriptor getUser1();

	public TGoogleUserDescriptor getUser2();

	public TGoogleUserDescriptor getUser3();

	public TGoogleUserDescriptor getUser4();

	public TGoogleUserDescriptor getUser5();

	public String getCredentialsFileLocation();
	
	public String getServiceAccountEmail();
	
	public String getHipChatToken();
	
	public String getHipChatServiceAccount();
		
}