package at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper;

import io.evanwong.oss.hipchat.v2.commons.GetRequest;

import org.apache.http.client.HttpClient;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class GetAllRoomMembersRequest extends GetRequest<Members> {

    private Integer startIndex;
    private Integer maxResults;    
    private final String roomIdOrName;

    GetAllRoomMembersRequest(String roomIdOrName, Integer startIndex, Integer maxResults, String accessToken, HttpClient httpClient, ExecutorService executorService) {
        super(accessToken, httpClient, executorService);
        this.startIndex = startIndex;
        this.maxResults = maxResults;        
        this.roomIdOrName = roomIdOrName;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public Integer getMaxResults() {
        return maxResults;
    }
   
    @Override
    protected String getPath() {
        return "/room/" + roomIdOrName + "/member" ;
    }

    @Override
    protected Map<String, Object> toQueryMap() {
        Map<String, Object> params = new HashMap<>();
        if (startIndex != null) {
            params.put("start-index", startIndex);
        }
        if (maxResults != null) {
            params.put("max-results", maxResults);
        }        
        return params;
    }
}
