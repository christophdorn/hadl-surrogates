package at.ac.tuwien.dsg.hadl.surrogates.hipchat;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rx.subjects.BehaviorSubject;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.OperationalStateTransitionControl;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.AbstractAsyncObjectSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ESurrogateStatus;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.HADLException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateErrorType;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateException;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.hipchat.THipChatRoomDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;
import at.ac.tuwien.dsg.hadl.surrogates.Collab;
import at.ac.tuwien.dsg.hadl.surrogates.gdrive.GoogleUtils;

public class HipChatRoomSurrogate extends AbstractAsyncObjectSurrogate {

	private static final Logger logger = LogManager.getLogger(HipChatRoomSurrogate.class);
		
	private HipChatConnector hcc = null;
	private THipChatRoomDescriptor rd;
	private ESurrogateStatus status = null;
	private ChangeSet cs = new ChangeSet();
	private boolean error = false;
	
	public HipChatRoomSurrogate(HipChatConnector hcc)
	{
		this.hcc = hcc;
	}

	@Override
	public void asyncBegin(BehaviorSubject<SurrogateEvent> subject) {				
		if (error) {
			subject.onCompleted();
			return;
		}
			
		if (cs != null)
		{					
			// hipchat logic here: only one owner, many admins, one creator
			// for now just add and remove regular users
			for (Map.Entry<TGoogleUserDescriptor, List<String>> entry : cs.toRemove.entrySet())
			{
				// as user can only be member once, just remove 
				hcc.removeUserFromRoom(entry.getKey().getEmail(), rd.getRoomId());
			}
			for (Map.Entry<TGoogleUserDescriptor, List<String>> entry : cs.toAdd.entrySet())
			{
				// user can only be member once, no handover of ownership available yet thus simple inviting
				hcc.inviteUserToRoom(entry.getKey().getEmail(), rd.getRoomId());
			}
		}
		cs = null;
		status = ESurrogateStatus.STARTING_SUCCESS;
		subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
		subject.onCompleted();
	}

	@Override
	public void asyncStop(BehaviorSubject<SurrogateEvent> subject) {
		if (error) {
			subject.onCompleted();
			return;
		}
		
		cs = new ChangeSet();			
		try {
			checkCapabilities();
			status = ESurrogateStatus.STOPPING_SUCCESS;
			subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
		} catch (SurrogateException e) {			
			status = ESurrogateStatus.STOPPING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, e, this.oc));			
		}		
		subject.onCompleted();			
	}

	@Override
	public void asyncRelease(BehaviorSubject<SurrogateEvent> subject) {
		if (error) {
			subject.onCompleted();
			return;
		}
		
		try
		{
			if (hcc.deleteRoom(rd))
			{
				this.oc.setState( OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.DESCRIBED_NONEXISTING) );
				status = ESurrogateStatus.RELEASING_SUCCESS;
				subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
			}			
		} catch (SurrogateException e)
		{
			this.oc.setState( OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.PRESCRIBED_NONEXISTING_DENIED) );
			status = ESurrogateStatus.RELEASING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, e, this.oc));
		}
		subject.onCompleted();
	}

	@Override
	public void asyncAcquire(TActivityScope forScope,
			TOperationalObject surrogateFor,
			BehaviorSubject<SurrogateEvent> subject) {
		if (error) {
			subject.onCompleted();
			return;
		}
		
		try{
			rd = HipChatUtils.extractRoomDescriptor(surrogateFor.getResourceDescriptor(), surrogateFor);
			if (rd.getRoomId() != null && rd.getRoomId().length() > 0)
			{
				if (checkCapabilities())
				{
					this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.DESCRIBED_EXISTING));
					status = ESurrogateStatus.ACQUIRING_SUCCESS;
					subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
				}
			}
			else
			{
				hcc.createRoom(rd);				
				//mode = SurrogateMode.SAVE;
				this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.DESCRIBED_EXISTING));
				status = ESurrogateStatus.ACQUIRING_SUCCESS;
				subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
			}
			
		} catch (HADLException e) {
			this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.PRESCRIBED_EXISTING_DENIED));
			status = ESurrogateStatus.ACQUIRING_FAILED;			
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(e.getMessage(), e, SurrogateErrorType.HADL_RETHROW)));
		} catch (SurrogateException e) {
			this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.PRESCRIBED_EXISTING_DENIED));
			status = ESurrogateStatus.ACQUIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, e));
		} 
		//cs = new ChangeSet();	
		subject.onCompleted();
	}

	@Override
	public void asyncLinkTo(TAction localAction,
			TOperationalConnector oppositeElement, TAction oppositeAction,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		// No connector known -- none so far
		status = ESurrogateStatus.REWIRING_FAILED;
		link.setState(OperationalStateTransitionControl.doTransition(link.getState(), TOperationalState.PRESCRIBED_EXISTING_DENIED));
		subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(MessageFormat.format("Wiring links to CollaborationConnector type {0} with id {1} unknown in hADLmodel", oppositeElement.getInstanceOf().getHref(), oppositeElement.getId()), SurrogateErrorType.NOT_POSSIBLE), link));
		subject.onCompleted();	
	}

	@Override
	public void asyncLinkTo(TAction localAction,
			TOperationalComponent oppositeElement, TAction oppositeAction,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		if (error) {
			subject.onCompleted();
			return;
		}
		
		try {
			//TODO: check if not linkage connector id
			TGoogleUserDescriptor fd = GoogleUtils.extractUserResourceDescriptor(oppositeElement.getResourceDescriptor(), oppositeElement);
		switch(localAction.getId())
		{
		case Collab.OACTION_ChatRoom_chat: //fall through		
		case Collab.OACTION_ChatRoom_coordinate:	
			addLink(fd, localAction.getId());			
			break;
		default:
			throw new SurrogateException(MessageFormat.format("Wiring links from Action type {0} not supported by HipChatRoomSurrogate", localAction.getId()), SurrogateErrorType.UNSUPPORTED_ACTION);
		}
		status = ESurrogateStatus.REWIRING_SUCCESS;
		subject.onNext(new SurrogateEvent(thisRef, status, link));
		
		} catch (SurrogateException e) {
			status = ESurrogateStatus.REWIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, e, link));			
		} catch (HADLException e) {			
			status = ESurrogateStatus.REWIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(e.getMessage(), e, SurrogateErrorType.HADL_RETHROW), link));
		}
		subject.onCompleted();
	}

	@Override
	public void asyncDisconnectFrom(TAction localAction,
			TOperationalComponent oppositeElement,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		if (error) {
			subject.onCompleted();
			return;
		}
		
		try {
			//TODO: check if not linkage connector id
			TGoogleUserDescriptor fd = GoogleUtils.extractUserResourceDescriptor(oppositeElement.getResourceDescriptor(), oppositeElement);
		switch(localAction.getId())
		{
		case Collab.OACTION_ChatRoom_chat: //fall through		
		case Collab.OACTION_ChatRoom_coordinate:	
			removeLink(fd, localAction.getId());
			break;
		default:
			throw new SurrogateException(MessageFormat.format("Wiring links from Action type {0} not supported by HipChatRoomSurrogate", localAction.getId()), SurrogateErrorType.UNSUPPORTED_ACTION);
		}
		status = ESurrogateStatus.REWIRING_SUCCESS;
		subject.onNext(new SurrogateEvent(thisRef, status, link));
		
		} catch (SurrogateException e) {
			status = ESurrogateStatus.REWIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, e, link));			
		} catch (HADLException e) {			
			status = ESurrogateStatus.REWIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(e.getMessage(), e, SurrogateErrorType.HADL_RETHROW), link));
		}
		subject.onCompleted();
	}

	@Override
	public void asyncDisconnectFrom(TAction localAction,
			TOperationalConnector oppositeElement,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		// No connector known -- none so far
		status = ESurrogateStatus.REWIRING_FAILED;
		link.setState(OperationalStateTransitionControl.doTransition(link.getState(), TOperationalState.PRESCRIBED_EXISTING_DENIED));
		subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(MessageFormat.format("Wiring links to CollaborationConnector type {0} with id {1} unknown in hADLmodel", oppositeElement.getInstanceOf().getHref(), oppositeElement.getId()), SurrogateErrorType.NOT_POSSIBLE), link));
		subject.onCompleted();	
	}

	@Override
	public void asyncRelating(TOperationalObject oppositeElement,
			TObjectRef relationType, boolean isRelationOrigin,
			boolean doRemove, BehaviorSubject<SurrogateEvent> subject,
			TOperationalObjectRef ref) {
		status = ESurrogateStatus.RELATING_FAILED;
		subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException("No such relation known", SurrogateErrorType.UNSPECIFIED), ref));
		subject.onCompleted();
	}
	
	private void removeLink(TGoogleUserDescriptor user, String hADLactionTypeId)
	{
		synchronized(cs)
		{
			if (cs.toAdd.containsKey(user))
			{
				if (cs.toAdd.get(user).contains(hADLactionTypeId)) // previous request to add
				{
					cs.toAdd.get(user).remove(hADLactionTypeId); // DONE, no need to add to add set				
					return;
				}
			}
			// not in add set
			if (cs.toRemove.containsKey(user))
			{
				if (cs.toRemove.get(user).contains(hADLactionTypeId)) // already there, duplicate request, ignoring
					return;
				else
				{
					cs.toRemove.get(user).add(hADLactionTypeId); // added to list to remove, DONE
					return;
				}
			}
			else // not in set at all yet
			{
				ArrayList<String> actions = new ArrayList<String>();
				actions.add(hADLactionTypeId);
				cs.toRemove.put(user, actions);
			}
		}		
	}
	
	private void addLink(TGoogleUserDescriptor user, String hADLactionTypeId)
	{
		// needs checking if room is public, if not, check whether user is team member
		synchronized(cs)
		{
			if (cs.toRemove.containsKey(user))
			{
				if (cs.toRemove.get(user).contains(hADLactionTypeId)) // previous request to remove
				{
					cs.toRemove.get(user).remove(hADLactionTypeId); // DONE, no need to add to add set				
					return;
				}
			}
			// not in remove set
			if (cs.toAdd.containsKey(user))
			{
				if (cs.toAdd.get(user).contains(hADLactionTypeId)) // already there, duplicate request, ignoring
					return;
				else
				{
					cs.toAdd.get(user).add(hADLactionTypeId); // added to list to add, DONE
					return;
				}
			}
			else // not in set at all yet
			{
				ArrayList<String> actions = new ArrayList<String>();
				actions.add(hADLactionTypeId);
				cs.toAdd.put(user, actions);
			}
		}							
	}
	
	private boolean checkCapabilities() throws SurrogateException
	{
		// check if Room exists
		if (hcc.fillRoomInfos(rd))
			return true;
		else
		{
			error = true;
			throw new SurrogateException(MessageFormat.format("Room with Id {0} with name {1} not found or accessible", rd.getRoomId(), rd.getName()), SurrogateErrorType.NOT_POSSIBLE);
		}
	}
	
	private class ChangeSet
	{
		Hashtable<TGoogleUserDescriptor, List<String>> toAdd = new Hashtable<TGoogleUserDescriptor, List<String>>();
		Hashtable<TGoogleUserDescriptor, List<String>> toRemove = new Hashtable<TGoogleUserDescriptor, List<String>>();
	}
}
