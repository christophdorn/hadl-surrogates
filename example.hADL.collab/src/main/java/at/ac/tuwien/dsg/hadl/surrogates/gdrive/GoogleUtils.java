package at.ac.tuwien.dsg.hadl.surrogates.gdrive;

import java.text.MessageFormat;
import java.util.List;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.HADLErrorType;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.HADLException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateErrorType;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateException;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;

public class GoogleUtils {

	public static TGoogleFileDescriptor extractFileResourceDescriptor(List<TResourceDescriptor> rds, THADLarchElement operationalEl) throws SurrogateException, HADLException
	{		
		if (rds.isEmpty())
			throw new HADLException(MessageFormat.format("{0} with id {1} has no resource descriptors", operationalEl.getClass().toString(), operationalEl.getId()), HADLErrorType.NO_RESOURCE_DESCRIPTOR);
		String nonMatchingDescriptors = "";
		for (TResourceDescriptor rd : rds)
		{
			if (rd instanceof TGoogleFileDescriptor)
			{
				return (TGoogleFileDescriptor) rd;							
			}
			else
				nonMatchingDescriptors += rd.getClass().toString()+" ";
		}
		throw new SurrogateException(MessageFormat.format("{0} with id {1} has no resource descriptor of type TGoogleFileDescriptor, only {2}", operationalEl.getClass().toString(), operationalEl.getId(), nonMatchingDescriptors), SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR);	
	}
	
	public static TGoogleUserDescriptor extractUserResourceDescriptor(List<TResourceDescriptor> rds, THADLarchElement operationalEl) throws SurrogateException, HADLException
	{		
		if (rds.isEmpty())
			throw new HADLException(MessageFormat.format("{0} with id {1} has no resource descriptors", operationalEl.getClass().toString(), operationalEl.getId()), HADLErrorType.NO_RESOURCE_DESCRIPTOR);
		String nonMatchingDescriptors = "";
		for (TResourceDescriptor rd : rds)
		{
			if (rd instanceof TGoogleUserDescriptor)
			{
				return (TGoogleUserDescriptor) rd;							
			}
			else
				nonMatchingDescriptors += rd.getClass().toString()+" ";
		}
		throw new SurrogateException(MessageFormat.format("{0} with id {1} has no resource descriptor of type TGoogleUserDescriptor, only {2}", operationalEl.getClass().toString(), operationalEl.getId(), nonMatchingDescriptors), SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR);	
	}

	public static TGoogleFileDescriptor getFileDescriptor(String filename, String fileId)
	{
		TGoogleFileDescriptor gdf = new TGoogleFileDescriptor();
		gdf.setName(filename); 
		gdf.setFileId(fileId);
		gdf.setId(fileId);
		gdf.getMimetype().add("application/vnd.google-apps.document");
		return gdf;
	}
	
	public static TGoogleUserDescriptor getUserDescriptor(String fullname, String userEmail)
	{
		TGoogleUserDescriptor rd = new TGoogleUserDescriptor();
		rd.setEmail(userEmail);
		rd.setFullname(fullname);
		if (userEmail != null && userEmail.indexOf('@') > 0)
		{
			rd.setName(userEmail.substring(0,userEmail.indexOf('@')));
		}
		rd.setId(userEmail);
		return rd;
	}
	
	public static TGoogleFileDescriptor getNewFileDescriptor(String filename)
	{
		TGoogleFileDescriptor gdf = new TGoogleFileDescriptor();
		gdf.setName(filename); 			
		gdf.setId("");
		gdf.getMimetype().add("application/vnd.google-apps.document");
		return gdf;
	}
}
