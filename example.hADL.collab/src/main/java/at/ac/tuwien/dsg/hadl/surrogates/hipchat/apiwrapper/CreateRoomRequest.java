package at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper;

import io.evanwong.oss.hipchat.v2.commons.PostRequest;
import io.evanwong.oss.hipchat.v2.rooms.Privacy;

import org.apache.http.client.HttpClient;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/*
The MIT License (MIT)

Copyright (c) 2014 Evan Wong

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// UPDATED to use RoomCreatedEntity
public class CreateRoomRequest  extends PostRequest<RoomCreatedEntity>{


	private String topic;
	private Boolean guestAcccess;
	private String name;
	//The id, email address, or mention name (beginning with an '@') of the room's owner. Defaults to the current user
	private String ownerUserId;
	private Privacy privacy;

	CreateRoomRequest(String topic, Boolean guestAcccess, String name, String ownerUserId, Privacy privacy, String accessToken, HttpClient httpClient, ExecutorService executorService) {
		super(accessToken, httpClient, executorService);
		if (name == null || name.length() == 0) {
			throw new IllegalArgumentException("name is required.");
		}
		this.topic = topic;
		this.guestAcccess = guestAcccess;
		this.name = name;
		this.ownerUserId = ownerUserId;
		this.privacy = privacy;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public void setGuestAcccess(Boolean guestAcccess) {
		this.guestAcccess = guestAcccess;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOwnerUserId(String ownerUserId) {
		this.ownerUserId = ownerUserId;
	}

	public void setPrivacy(Privacy privacy) {
		this.privacy = privacy;
	}

	@Override
	protected String getPath() {
		return "/room";
	}

	@Override
	protected Map<String, Object> toQueryMap() {
		Map<String, Object> params = new HashMap<>();
		if (topic != null) {
			params.put("topic", topic);
		}
		if (guestAcccess != null) {
			params.put("guest_acccess", guestAcccess);
		}
		params.put("name", name);
		if (ownerUserId != null) {
			params.put("owner_user_id", ownerUserId);
		}
		if (privacy != null) {
			params.put("privacy", privacy.getValue());
		}
		return params;
	}
}

