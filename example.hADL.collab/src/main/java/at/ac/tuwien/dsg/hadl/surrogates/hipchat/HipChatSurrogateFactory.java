package at.ac.tuwien.dsg.hadl.surrogates.hipchat;


import javax.inject.Inject;
import javax.xml.bind.JAXBElement;

import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ICollaboratorSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.IObjectSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ISurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.utils.DefaultAcceptingCollaboratorSurrogate;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement.Extension;
import at.ac.tuwien.dsg.hadl.schema.executable.TCollabPlatform;
import at.ac.tuwien.dsg.hadl.schema.executable.TExecutableRefExtension;
import at.ac.tuwien.dsg.hadl.schema.executable.TSurrogate;
import at.ac.tuwien.dsg.hadl.schema.extension.platform.hipchat.THipChatCollabPlatform;
import at.ac.tuwien.dsg.hadl.surrogates.Collab;
import at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper.ExtendedHipChatClient;


public class HipChatSurrogateFactory implements SurrogateFactory{

	@Inject private ModelTypesUtil mtu;

	private HipChatConnector ds = null;		
	
	@Override
	public ISurrogate getInstance(THADLarchElement type)
			throws InsufficientModelInformationException {
		if (type instanceof TCollaborator)
		{
			return getInstance((TCollaborator)type);
		}
		else if (type instanceof TCollabObject)
		{
			return getInstance((TCollabObject)type);
		}
		else
			throw new InsufficientModelInformationException(type.getId()+"not supported", type.getId());
	}

	@Override
	public ICollaboratorSurrogate getInstance(TCollaborator collaboratorSpec)
			throws InsufficientModelInformationException {
		
		switch(collaboratorSpec.getId())
		{
		case Collab.COMP_ChatUser:		
			return new DefaultAcceptingCollaboratorSurrogate();
		default: throw new InsufficientModelInformationException(collaboratorSpec.getId()+" not Implemented", collaboratorSpec.getId());
		}
	}

	@Override
	public IObjectSurrogate getInstance(TCollabObject objectSpec)
			throws InsufficientModelInformationException {
		
		switch(objectSpec.getId())
		{
		case Collab.STM_ChatRoom:
			return new HipChatRoomSurrogate(getHipChatService(objectSpec));
		default: throw new InsufficientModelInformationException(objectSpec.getId()+" not Implemented", objectSpec.getId());
		}
			
	}

	private HipChatConnector getHipChatService(THADLarchElement el) throws InsufficientModelInformationException
	{
		if (ds == null)
		{
			extractSurrogateConfig(el);
		}
		return ds;
	}
	
	@SuppressWarnings("rawtypes")
	private void extractSurrogateConfig(THADLarchElement el) throws InsufficientModelInformationException
	{						
		for (Extension ext : el.getExtension())
		{
			Object obj2 = ext.getAny();
			if (obj2 instanceof JAXBElement)
				obj2 = ((JAXBElement)obj2).getValue();					
			if (obj2 instanceof TExecutableRefExtension)
			{									
				for (Object obj : ((TExecutableRefExtension)obj2).getExecutableViaSurrogate())
				{
					TSurrogate surr = null;					
					if (obj instanceof TSurrogate)
					{
						surr = (TSurrogate)obj;
						for (TCollabPlatform platform : surr.getCollabPlatform())
						{							
							if (platform instanceof THipChatCollabPlatform)
							{
								String token = ((THipChatCollabPlatform) platform).getToken();
								String account = ((THipChatCollabPlatform) platform).getServiceAccount();
								if (!(token == null || token.length() <= 0 || account == null || account.length() <= 0))
								{									
									try {
										
										ds = new HipChatConnector(new ExtendedHipChatClient(token));
										return;
									} catch (Exception e) {
										throw new InsufficientModelInformationException("HipChat CollabPlatform API access unsuccessful: "+e.getMessage(), THipChatCollabPlatform.class.getCanonicalName());
										
									} 								
								}
							}
						}
					}
				}					
			}			
		}
		
			throw new InsufficientModelInformationException("No HipChat CollabPlatform API access credentials configuration found", THipChatCollabPlatform.class.getCanonicalName());	
	}
}
