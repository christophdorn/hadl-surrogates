package at.ac.tuwien.dsg.hadl.surrogates.hipchat;



import java.text.MessageFormat;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.HADLException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateErrorType;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateException;
import at.ac.tuwien.dsg.hadl.schema.extension.hipchat.THipChatRoomDescriptor;
import at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper.ExtendedHipChatClient;
import at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper.Members;
import at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper.RoomCreatedEntity;
import io.evanwong.oss.hipchat.v2.commons.NoContent;
import io.evanwong.oss.hipchat.v2.rooms.Privacy;
import io.evanwong.oss.hipchat.v2.rooms.Room;

public class HipChatConnector 
{

	private ExtendedHipChatClient hcc;
	
	public HipChatConnector(ExtendedHipChatClient hcc)
	{
		this.hcc = hcc;		
	}
	
	public void createRoom(THipChatRoomDescriptor rd) throws SurrogateException
	{
		try {
			RoomCreatedEntity response = hcc.prepareCreateRoomRequestBuilder(rd.getName())				
				.setGuestAcccess(rd.isGuestaccess())
				.setPrivacy(!rd.getPrivacy().equals(Privacy.PUBLIC) ? Privacy.PRIVATE : Privacy.PUBLIC)
				.setTopic(rd.getTopic() != null ? rd.getTopic() : "")
				.build().execute().get();			
			Long id =  response.getEntity().getId();
			rd.setRoomId(id.toString());
			rd.setId(id.toString());
		} catch (Exception e) {			
			throw new SurrogateException(MessageFormat.format("CreatingRoom with name {0} failed", rd.getName()), e, SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC);
		} 	
	}
	
	public Long createRoom(String name)
	{
		try {
			RoomCreatedEntity response = hcc.prepareCreateRoomRequestBuilder(name)				
				.setGuestAcccess(false)
				.setPrivacy(Privacy.PRIVATE)
				.setTopic("TestTopic")
				.build().execute().get();			
			return response.getEntity().getId();
		} catch (Exception e) {			
			e.printStackTrace();
			return -1l;
		} 		
	}
	
	
	public boolean deleteRoom(THipChatRoomDescriptor rd) throws SurrogateException
	{
		try {
			NoContent response = hcc.prepareDeleteRoomRequestBuilder(rd.getRoomId()).build().execute().get();
			return true;
		} catch (Exception e) {			
			throw new SurrogateException(MessageFormat.format("Deleting Room with name {0} failed", rd.getName()), e, SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC);
		}
	}
	
	public boolean deleteRoom(String roomIdOrName)
	{
		try {
			NoContent response = hcc.prepareDeleteRoomRequestBuilder(roomIdOrName).build().execute().get();
			return true;
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}
	}
	
	public Members.Item[] getRoomMembers(String roomId)
	{
		try{			
			Members members = hcc.prepareGetAllRoomMembersRequestBuilder(roomId).build().execute().get();			
			return members.getItems();
		} catch (Exception e) {			
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean inviteUserToRoom(String userEmail, String roomId)
	{
		try{					
			NoContent response = hcc.prepareAddRoomMemberRequestBuilder(userEmail, roomId).build().execute().get();			
			return true;
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}		
	}		
	
	public boolean removeUserFromRoom(String userEmail, String roomId)
	{
		try{						
			NoContent response = hcc.prepareRemoveRoomMemberRequestBuilder(userEmail, roomId).build().execute().get();
			return true;
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}	
	}
	
	public boolean fillRoomInfos(THipChatRoomDescriptor rd)
	{	
		try{
			Room room = hcc.prepareGetRoomRequestBuilder(rd.getRoomId()).build().execute().get();
			if (room != null)
			{
				rd.setGuestaccess(room.getIsGuestAccessible());
				rd.setName(room.getName());
				rd.setTopic(room.getTopic());
				rd.setPrivacy(room.getPrivacy().getValue());
				rd.setURL(room.getGuestAccessUrl());
				return true;
			}
			return false;
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}
	}	
}
