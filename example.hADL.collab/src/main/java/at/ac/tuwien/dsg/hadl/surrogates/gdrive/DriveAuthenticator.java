package at.ac.tuwien.dsg.hadl.surrogates.gdrive;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;

import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;

public class DriveAuthenticator {
	
	
	private static JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();	
	private static final HttpTransport TRANSPORT = new NetHttpTransport();
	    
	
	public static Drive authenticate(File secrets, String serviceAccountEmail) throws GeneralSecurityException, IOException
	{							
		 GoogleCredential credential = new GoogleCredential.Builder()
         .setTransport(TRANSPORT)
         .setJsonFactory(JSON_FACTORY)
         .setServiceAccountId(serviceAccountEmail)
         .setServiceAccountScopes(Arrays.asList(DriveScopes.DRIVE))
         .setServiceAccountPrivateKeyFromP12File(secrets)
         .build();
 

		 return new Drive.Builder(TRANSPORT, JSON_FACTORY, null)
         .setHttpRequestInitializer(credential).setApplicationName("hADL for Google Drive").build();
				
	}
	

	
}
