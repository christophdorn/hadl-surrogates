package at.ac.tuwien.dsg.hadl.surrogates.hipchat;

import java.text.MessageFormat;
import java.util.List;

import at.ac.tuwien.dsg.hadl.framework.runtime.intf.HADLErrorType;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.HADLException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateErrorType;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateException;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.extension.hipchat.THipChatRoomDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;

public class HipChatUtils {

	public static THipChatRoomDescriptor extractRoomDescriptor(List<TResourceDescriptor> rds, THADLarchElement operationalEl) throws SurrogateException, HADLException
	{
		if (rds.isEmpty())
			throw new HADLException(MessageFormat.format("{0} with id {1} has no resource descriptors", operationalEl.getClass().toString(), operationalEl.getId()), HADLErrorType.NO_RESOURCE_DESCRIPTOR);
		String nonMatchingDescriptors = "";
		for (TResourceDescriptor rd : rds)
		{
			if (rd instanceof THipChatRoomDescriptor)
			{
				return (THipChatRoomDescriptor) rd;							
			}
			else
				nonMatchingDescriptors += rd.getClass().toString()+" ";
		}
		throw new SurrogateException(MessageFormat.format("{0} with id {1} has no resource descriptor of type THipChatRoomDescriptor, only {2}", operationalEl.getClass().toString(), operationalEl.getId(), nonMatchingDescriptors), SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR);
	}
	
	
	public static THipChatRoomDescriptor getNewRoomDescriptor(String roomName, String roomTopic)
	{
		THipChatRoomDescriptor rd = new THipChatRoomDescriptor();
		rd.setGuestaccess(false);
		rd.setPrivacy("PRIVATE");
		rd.setName(roomName);
		rd.setTopic(roomTopic);
		rd.setId("");
		return rd;
	}
	
	public static THipChatRoomDescriptor getRoomDescriptor(String roomId)
	{
		THipChatRoomDescriptor rd = new THipChatRoomDescriptor();		
		rd.setId(roomId);
		rd.setRoomId(roomId);
		return rd;
	}
}
