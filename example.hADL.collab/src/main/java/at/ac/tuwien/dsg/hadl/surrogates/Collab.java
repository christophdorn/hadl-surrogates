package at.ac.tuwien.dsg.hadl.surrogates;		

public class Collab {			
public static final String MODEL_collab = "collab";


public static final String STRUCT_doc = "collab.doc";  

		
		public static final String COMP_User = "collab.doc.User"; 
		public static final String CACTION_User_own = "collab.doc.User.own";
		public static final String CACTION_User_edit = "collab.doc.User.edit";
		public static final String CACTION_User_read = "collab.doc.User.read";
		public static final String CACTION_User_comment = "collab.doc.User.comment";
//		public static final String CACTION_User_ownFolder = "collab.doc.User.ownFolder";
//		public static final String CACTION_User_editFolder = "collab.doc.User.editFolder";
//		public static final String CACTION_User_readFolder = "collab.doc.User.readFolder";
		
		public static final String ART_File = "collab.doc.File"; 
		public static final String OACTION_File_own = "collab.doc.File.own";
		public static final String OACTION_File_edit = "collab.doc.File.edit";
		public static final String OACTION_File_read = "collab.doc.File.read";
		public static final String OACTION_File_comment = "collab.doc.File.comment";
		
//		public static final String ART_Folder = "collab.doc.Folder"; 
//		public static final String OACTION_Folder_own = "collab.doc.Folder.own";
//		public static final String OACTION_Folder_edit = "collab.doc.Folder.edit";
//		public static final String OACTION_Folder_read = "collab.doc.Folder.read";
		public static final String LINK_owning = "collab.doc.owning";
		public static final String LINK_editing = "collab.doc.editing";
		public static final String LINK_reading = "collab.doc.reading";
		public static final String LINK_commenting = "collab.doc.commenting";
//		public static final String LINK_owningFolder = "collab.doc.owningFolder";
//		public static final String LINK_editingFolder = "collab.doc.editingFolder";
//		public static final String LINK_readingFolder = "collab.doc.readingFolder";
//		public static final String OREF_folderInFolder = "collab.doc.folderInFolder";
//		public static final String OREF_fileInFolder = "collab.doc.fileInFolder";
//		public static final String OREF_templateFile = "collab.doc.templateFile";
		public static final String SCOPE_fullScope = "collab.doc.fullScope";

public static final String STRUCT_chat = "collab.chat";  

		
		public static final String COMP_ChatUser = "collab.chat.ChatUser"; 
		public static final String CACTION_ChatUser_chat = "collab.chat.ChatUser.chat";
		public static final String CACTION_ChatUser_coordinate = "collab.chat.ChatUser.coordinate";
		
//		public static final String CONN_ChatAdmin = "collab.chat.ChatAdmin"; 
//		public static final String CACTION_ChatAdmin_coordinate = "collab.chat.ChatAdmin.coordinate";
//		public static final String CACTION_ChatAdmin_establish = "collab.chat.ChatAdmin.establish";
		
		public static final String STM_ChatRoom = "collab.chat.ChatRoom"; 
		public static final String OACTION_ChatRoom_chat = "collab.chat.ChatRoom.chat";
		public static final String OACTION_ChatRoom_coordinate = "collab.chat.ChatRoom.coordinate";
//		public static final String OACTION_ChatRoom_establish = "collab.chat.ChatRoom.establish";
		public static final String LINK_chatting = "collab.chat.chatting";
		public static final String LINK_owningRoom = "collab.chat.owningRoom";
//		public static final String LINK_coordinating = "collab.chat.coordinating";
//		public static final String LINK_establishing = "collab.chat.establishing";
		public static final String SCOPE_fullChatScope = "collab.chat.fullChatScope";
}
