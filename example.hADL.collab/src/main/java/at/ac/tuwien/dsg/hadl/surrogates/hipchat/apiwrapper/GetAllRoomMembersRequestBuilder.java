package at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper;

import io.evanwong.oss.hipchat.v2.commons.ExpandableRequestBuilder;
import io.evanwong.oss.hipchat.v2.commons.RequestBuilder;

import org.apache.http.client.HttpClient;

import java.util.concurrent.ExecutorService;

//public class GetAllRoomMembersRequestBuilder extends ExpandableRequestBuilder<GetAllRoomMembersRequestBuilder, GetAllRoomMembersRequest> {
public class GetAllRoomMembersRequestBuilder extends RequestBuilder<GetAllRoomMembersRequest> {

    private Integer startIndex;
    private Integer maxResults;    
    private String roomIdOrName;

    public GetAllRoomMembersRequestBuilder(String roomIdOrName, String accessToken, HttpClient httpClient, ExecutorService executorService) {
        super(accessToken, httpClient, executorService);
        this.roomIdOrName = roomIdOrName;
    }

    public GetAllRoomMembersRequestBuilder setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
        return this;
    }

    public GetAllRoomMembersRequestBuilder setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
        return this;
    }

    public GetAllRoomMembersRequestBuilder setRoomIdOrName(String roomIdOrName) {
        this.roomIdOrName = roomIdOrName;
        return this;
    }

    @Override
    public GetAllRoomMembersRequest build() {
        if (accessToken == null) {
            throw new IllegalArgumentException("accessToken is required");
        }
        if (roomIdOrName == null) {
        	throw new IllegalArgumentException("roomIdOrName is required");
        }
        GetAllRoomMembersRequest getAllRoomsRequest = new GetAllRoomMembersRequest(roomIdOrName, startIndex, maxResults, accessToken, httpClient, executorService);
//        if (!expansions.isEmpty()) {
//            expansions.forEach(title -> getAllRoomsRequest.addExpansion(title));
//        }
        return getAllRoomsRequest;
    }
}
