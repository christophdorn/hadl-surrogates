package at.ac.tuwien.dsg.hadl.surrogates.gdrive;

import java.io.IOException;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;

import com.google.api.client.http.HttpResponseException;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.PermissionList;
import com.google.api.services.drive.model.User;

/**
 * Service class which offers methods for sending requests to Google Drive.
 *
 */
public class DriveService {

    private Drive gdrive;
    private String serviceAccountEmailAddress;
    
    private static final Logger logger = LogManager.getLogger(DriveService.class);

    public DriveService(Drive withDrive, String serviceAccountEmailAddress){ 
    	this.gdrive = withDrive;
    	this.serviceAccountEmailAddress = serviceAccountEmailAddress;
    }

    
    public void createFile(TGoogleFileDescriptor toCreate) throws IOException
    {
    	File f = new File();        
        f.setTitle(toCreate.getName());     
        //TODO: search through MimeTypes for GoogleDrive specific ones, not vital/rather a 'nice to have'
        f.setMimeType(toCreate.getMimetype().isEmpty() ? "text/plain" : toCreate.getMimetype().get(0));        
        f = this.gdrive.files().insert(f).execute();
        // update FileDescriptor
        toCreate.setId(f.getId());        
        toCreate.setFileId(f.getId());
        if (toCreate.getMimetype().isEmpty())
        	toCreate.getMimetype().add(f.getMimeType());
        toCreate.setURL(f.getSelfLink());
        toCreate.setPublicURL(f.getWebContentLink()); //rather the download link
    }
    
    public boolean fileExists(TGoogleFileDescriptor toCheck) throws IOException
    {
    	try{
    		File f = this.gdrive.files().get(toCheck.getFileId()).execute();
    		return f != null;
    	}
    	catch (HttpResponseException e) {            
            if(e.getStatusCode() == 404) {
                return false;
            }
            else
            	throw e;
        }
    }
    
    public boolean removeFile(TGoogleFileDescriptor toRemove) throws IOException
    {
    	if (fileExists(toRemove))
    	{
    		this.gdrive.files().trash(toRemove.getFileId()).execute();
    		return true;
    	}
    	else 
    		return true;	
    }
    
    public File getFileHandle(TGoogleFileDescriptor toGet) throws IOException
    {    	
    		File f = this.gdrive.files().get(toGet.getFileId()).execute();
    		return f;
    }
    
    public boolean isOwner(File f) throws IOException
    {    	
    	for(User u : f.getOwners())
    	{
    		if (u.getEmailAddress().equalsIgnoreCase(serviceAccountEmailAddress))
    			return true;
    	}
    	return false;
    }

    public void grantPermission(TGoogleFileDescriptor file, TGoogleUserDescriptor user, DriveFilePermission permissionToGrant) throws IOException 
    {               
    	Permission p = new Permission();
    	p.setValue(user.getEmail());
    	if (permissionToGrant.equals(DriveFilePermission.commenter))
		{
    		p.setRole(DriveFilePermission.reader.toString());
    		p.setAdditionalRoles(Arrays.asList(new String[]{DriveFilePermission.commenter.toString()}));
		}
    	else
    	{
    		p.setRole(permissionToGrant.toString());
    	}    	
		p.setType("user");
    	this.gdrive.permissions().insert(file.getFileId(), p).execute();
    }
    
    public void removePermission(TGoogleFileDescriptor file, TGoogleUserDescriptor user) throws IOException 
    {       
    	Permission p = this.getPermission(file, user);
    	if (p != null)
    	{
          this.gdrive.permissions().delete(file.getFileId(), p.getId()).execute();
    	}
    }

    public Permission getPermission(TGoogleFileDescriptor file, TGoogleUserDescriptor user) throws IOException 
    {
        Permission result = null;                            
        PermissionList list = this.gdrive.permissions().list(file.getFileId()).execute();
        for(Permission p : list.getItems()) {
        	if(p.getEmailAddress().equals(user.getEmail())) {
        		result = p;
        	}
        }
        return result;
    }
    
    public boolean hasPermission(TGoogleFileDescriptor file, TGoogleUserDescriptor user, DriveFilePermission permissionToCheck) throws IOException
    {    	                  
        PermissionList list = this.gdrive.permissions().list(file.getFileId()).execute();
        for(Permission p : list.getItems()) {
        	if(p.getEmailAddress().equals(user.getEmail())) {
        		DriveFilePermission dp = DriveFilePermission.valueOf(p.getRole());
        		if (permissionToCheck.equals(DriveFilePermission.commenter))
        		{
        			for (String otherRole : p.getAdditionalRoles())
        			{
        				if (otherRole.equals(DriveFilePermission.commenter.toString()))
        					return true;
        			}
        			return false;
        		}
        		else
        			return dp.equals(permissionToCheck);
        	}
        }           
    	return false;
    }

    	
    
}
