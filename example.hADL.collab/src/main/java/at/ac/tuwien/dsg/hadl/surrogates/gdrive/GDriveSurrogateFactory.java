package at.ac.tuwien.dsg.hadl.surrogates.gdrive;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.AbstractMap;

import javax.inject.Inject;
import javax.xml.bind.JAXBElement;

import com.google.api.services.drive.Drive;

import example.gdrive.TestGoogleDriveConnector;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ICollaboratorSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.IObjectSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ISurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.utils.DefaultAcceptingCollaboratorSurrogate;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement.Extension;
import at.ac.tuwien.dsg.hadl.schema.executable.TCollabPlatform;
import at.ac.tuwien.dsg.hadl.schema.executable.TExecutableRefExtension;
import at.ac.tuwien.dsg.hadl.schema.executable.TSurrogate;
import at.ac.tuwien.dsg.hadl.schema.extension.platform.google.TGDriveCollabPlatform;
import at.ac.tuwien.dsg.hadl.surrogates.Collab;


public class GDriveSurrogateFactory implements SurrogateFactory{

	@Inject private ModelTypesUtil mtu;

	private DriveService ds = null;		
	
	@Override
	public ISurrogate getInstance(THADLarchElement type)
			throws InsufficientModelInformationException {
		if (type instanceof TCollaborator)
		{
			return getInstance((TCollaborator)type);
		}
		else if (type instanceof TCollabObject)
		{
			return getInstance((TCollabObject)type);
		}
		else
			throw new InsufficientModelInformationException(type.getId()+"not supported", type.getId());
	}

	@Override
	public ICollaboratorSurrogate getInstance(TCollaborator collaboratorSpec)
			throws InsufficientModelInformationException {
		
		switch(collaboratorSpec.getId())
		{
		case Collab.COMP_User:
			return new DefaultAcceptingCollaboratorSurrogate();
		default: throw new InsufficientModelInformationException(collaboratorSpec.getId()+" not Implemented", collaboratorSpec.getId());
		}
	}

	@Override
	public IObjectSurrogate getInstance(TCollabObject objectSpec)
			throws InsufficientModelInformationException {
		
		switch(objectSpec.getId())
		{
		case Collab.ART_File:
			return new GDriveFileSurrogate(getDriveService(objectSpec));
		default: throw new InsufficientModelInformationException(objectSpec.getId()+" not Implemented", objectSpec.getId());
		}
			
	}

	private DriveService getDriveService(THADLarchElement el) throws InsufficientModelInformationException
	{
		if (ds == null)
		{
			extractSurrogateConfig(el);
		}
		return ds;
	}
	
	@SuppressWarnings("rawtypes")
	private void extractSurrogateConfig(THADLarchElement el) throws InsufficientModelInformationException
	{						
		for (Extension ext : el.getExtension())
		{
			Object obj2 = ext.getAny();
			if (obj2 instanceof JAXBElement)
				obj2 = ((JAXBElement)obj2).getValue();					
			if (obj2 instanceof TExecutableRefExtension)
			{									
				for (Object obj : ((TExecutableRefExtension)obj2).getExecutableViaSurrogate())
				{
					TSurrogate surr = null;					
					if (obj instanceof TSurrogate)
					{
						surr = (TSurrogate)obj;
						for (TCollabPlatform platform : surr.getCollabPlatform())
						{							
							if (platform instanceof TGDriveCollabPlatform)
							{
								String filename = ((TGDriveCollabPlatform) platform).getCredentialsFile();
								String account = ((TGDriveCollabPlatform) platform).getServiceAccount();
								if (!(filename == null || filename.length() <= 0 || account == null || account.length() <= 0))
								{									
									try {
										Drive drive = DriveAuthenticator.authenticate(new java.io.File(TestGoogleDriveConnector.class.getClassLoader().getResource(filename).toURI()), account);
										ds = new DriveService(drive, account);
										return;
									} catch (Exception e) {
										throw new InsufficientModelInformationException("GoogleDrive CollabPlatform API access unsuccessful: "+e.getMessage(), TGDriveCollabPlatform.class.getCanonicalName());
										
									} 								
								}
							}
						}
					}
				}					
			}			
		}
		
			throw new InsufficientModelInformationException("No GoogleDrive CollabPlatform API access credentials configuration found", TGDriveCollabPlatform.class.getCanonicalName());	
	}
}
