package at.ac.tuwien.dsg.hadl.surrogates.gdrive;

import at.ac.tuwien.dsg.hadl.surrogates.Collab;

public enum DriveFilePermission {	
	owner(Collab.OACTION_File_own),
	writer(Collab.OACTION_File_edit),	
	commenter(Collab.OACTION_File_comment),	
	reader(Collab.OACTION_File_read)	
	;
	
	private String hADLaction;
	
	private DriveFilePermission(String hADLactionId)
	{
		this.hADLaction = hADLactionId;
	}
	
	public String getHADLActionId()
	{
		return hADLaction;
	}
}
