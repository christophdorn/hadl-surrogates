package at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.evanwong.oss.hipchat.v2.commons.Links;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomCreatedEntity {
	
	private Created entity;

	public Created getEntity() {
		return entity;
	}

	public void setEntity(Created entity) {
		this.entity = entity;
	}
	

	
	public class Created{
	    private Long id;
	    private Links links;
	    private String name;
	    private String version;

	    public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }

	    public Links getLinks() {
	        return links;
	    }

	    public void setLinks(Links links) {
	        this.links = links;
	    }

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}
	    
	    
	}
}
