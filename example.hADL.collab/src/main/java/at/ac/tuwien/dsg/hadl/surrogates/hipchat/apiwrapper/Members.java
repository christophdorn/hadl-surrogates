package at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Members {

	private Item[] items;
	
	public Item[] getItems() {
		return items;
	}

	public void setItems(Item[] items) {
		this.items = items;
	}

	


	@Override
	public String toString() {
		return "Members [items=" + Arrays.toString(items) + "]";
	}




	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Item
	{
		private String mention_name;
		private String id;
		private String name;
		
		public String getMention_name() {
			return mention_name;
		}
		public void setMention_name(String mention_name) {
			this.mention_name = mention_name;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return "Item [mention_name=" + mention_name + ", id=" + id
					+ ", name=" + name + "]";
		}
		
		
	}
	
}
