package at.ac.tuwien.dsg.hadl.surrogates.gdrive;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.drive.model.File;

import rx.subjects.BehaviorSubject;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.OperationalStateTransitionControl;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.AbstractAsyncObjectSurrogate;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.ESurrogateStatus;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.HADLException;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateErrorType;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.SurrogateException;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleFileDescriptor;
import at.ac.tuwien.dsg.hadl.schema.extension.google.TGoogleUserDescriptor;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;
import at.ac.tuwien.dsg.hadl.surrogates.Collab;

public class GDriveFileSurrogate extends AbstractAsyncObjectSurrogate {

	  private static final Logger logger = LogManager.getLogger(GDriveFileSurrogate.class);
	
	private SurrogateMode mode = SurrogateMode.DEFUNCT;
	private DriveService ds = null;
	private TGoogleFileDescriptor gfd;
	private ESurrogateStatus status = null;
	private ChangeSet cs = new ChangeSet();
	
	public GDriveFileSurrogate(DriveService ds)
	{
		this.ds = ds;
	}
	
	@Override
	public void asyncBegin(BehaviorSubject<SurrogateEvent> subject) {		
		List<Exception> ex = new ArrayList<Exception>();
		if (cs != null)
		{					
			if ((cs.toAdd.size() > 0 || cs.toRemove.size() > 0)) // only if there are actual changes to do, else don't check
			{
				if (mode.compareTo(SurrogateMode.BRITTLE) > 0) // not able to effect permission changes
				{
					cs = null;
					status = ESurrogateStatus.STARTING_FAILED;
					subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(MessageFormat.format("Surrogate has insufficient permissions to execute rewiring on File {0}", gfd.getFileId()), SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC)));
					subject.onCompleted();
					return;
				}
			}
			for (Map.Entry<TGoogleUserDescriptor, List<DriveFilePermission>> entry : cs.toRemove.entrySet())
			{
				for (DriveFilePermission p : entry.getValue())
				{
					try {
						if (ds.hasPermission(gfd, entry.getKey(), p))
							ds.removePermission(gfd, entry.getKey());
					} catch (IOException e) {
						logger.info(MessageFormat.format("Error removing permission {0} of user {1} from file {2}", p.toString(), entry.getKey().getEmail(), gfd.getFileId()), e);
						ex.add(new SurrogateException(MessageFormat.format("Error removing permission {0} of user {1} from file {2}", p.toString(), entry.getKey().getEmail(), gfd.getFileId()), e, SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC));
						continue;
					}
				}
			}
			for (Map.Entry<TGoogleUserDescriptor, List<DriveFilePermission>> entry : cs.toAdd.entrySet())
			{
				Collections.sort(entry.getValue()); 
				for (DriveFilePermission p : entry.getValue())
				{
					try {						
						ds.grantPermission(gfd, entry.getKey(), p);
						break; // only grand first, the highest.
					} catch (IOException e) {
						logger.info(MessageFormat.format("Error adding permission {0} for user {1} on file {2}", p.toString(), entry.getKey().getEmail(), gfd.getFileId()), e);
						ex.add(new SurrogateException(MessageFormat.format("Error adding permission {0} for user {1} on file {2}", p.toString(), entry.getKey().getEmail(), gfd.getFileId()), e, SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC));
						continue;
					}
				}
			}
		}
		cs = null;
		status = ESurrogateStatus.STARTING_SUCCESS;
		subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
		subject.onCompleted();
	}

	@Override
	public void asyncStop(BehaviorSubject<SurrogateEvent> subject) {		
		cs = new ChangeSet();			
		try {
			checkCapabilities();
			status = ESurrogateStatus.STOPPING_SUCCESS;
			subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
		} catch (SurrogateException | IOException e) {			
			status = ESurrogateStatus.STOPPING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(MessageFormat.format("Error stopping file {0}", gfd.getFileId()), SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC), this.oc));			
		}		
		subject.onCompleted();		
	}

	@Override
	public void asyncRelease(BehaviorSubject<SurrogateEvent> subject) {
		
		try
		{
			if (ds.removeFile(gfd))
			{
				this.oc.setState( OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.DESCRIBED_NONEXISTING) );
				status = ESurrogateStatus.RELEASING_SUCCESS;
				subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
			}
			else
			{
				this.oc.setState( OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.PRESCRIBED_NONEXISTING_DENIED) );
				status = ESurrogateStatus.RELEASING_FAILED;
				subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(MessageFormat.format("Error trashing {0}", gfd.getFileId()), SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC), this.oc));
			}
		} catch (IOException e)
		{
			this.oc.setState( OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.PRESCRIBED_NONEXISTING_DENIED) );
			status = ESurrogateStatus.RELEASING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(MessageFormat.format("Error trashing {0}", this.oc.getId()), e, SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC), this.oc));
		}
		subject.onCompleted();
	}

	@Override
	public void asyncAcquire(TActivityScope forScope,
			TOperationalObject surrogateFor,
			BehaviorSubject<SurrogateEvent> subject) {				
		try{
			gfd = GoogleUtils.extractFileResourceDescriptor(surrogateFor.getResourceDescriptor(), surrogateFor);
			if (gfd.getFileId() != null && gfd.getFileId().length() > 0)
			{
				if (checkCapabilities())
				{
					this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.DESCRIBED_EXISTING));
					status = ESurrogateStatus.ACQUIRING_SUCCESS;
					subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
				}
			}
			else
			{
				ds.createFile(gfd);
				mode = SurrogateMode.SAVE;
				this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.DESCRIBED_EXISTING));
				status = ESurrogateStatus.ACQUIRING_SUCCESS;
				subject.onNext(new SurrogateEvent(thisRef, status, this.oc));
			}
			
		} catch (HADLException e) {
			this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.PRESCRIBED_EXISTING_DENIED));
			status = ESurrogateStatus.ACQUIRING_FAILED;			
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(e.getMessage(), e, SurrogateErrorType.HADL_RETHROW)));
		} catch (SurrogateException e) {
			this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.PRESCRIBED_EXISTING_DENIED));
			status = ESurrogateStatus.ACQUIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, e));
		} catch (IOException e) {
			this.oc.setState(OperationalStateTransitionControl.doTransition(this.oc.getState(), TOperationalState.PRESCRIBED_EXISTING_DENIED));
			status = ESurrogateStatus.ACQUIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(e.getMessage(), e, SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC)));		
		}
		cs = new ChangeSet();	
		subject.onCompleted();
	}

	@Override
	public void asyncLinkTo(TAction localAction,
			TOperationalConnector oppositeElement, TAction oppositeAction,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		// No connector known -- none so far
		status = ESurrogateStatus.REWIRING_FAILED;
		link.setState(OperationalStateTransitionControl.doTransition(link.getState(), TOperationalState.PRESCRIBED_EXISTING_DENIED));
		subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(MessageFormat.format("Wiring links to CollaborationConnector type {0} with id {1} unknown in hADLmodel", oppositeElement.getInstanceOf().getHref(), oppositeElement.getId()), SurrogateErrorType.NOT_POSSIBLE), link));
		subject.onCompleted();
	}

	@Override
	public void asyncLinkTo(TAction localAction,
			TOperationalComponent oppositeElement, TAction oppositeAction,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		
		try {
			if (mode.compareTo(SurrogateMode.BRITTLE) > 0) // not able to effect permission changes
			{	
				throw new SurrogateException(MessageFormat.format("Surrogate has insufficient permissions to execute rewiring on File {0}", gfd.getFileId()), SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC);
			}
			//assuming opposite user exists due to linkage connector logic and has all required info (i.e., email address)
			TGoogleUserDescriptor fd = GoogleUtils.extractUserResourceDescriptor(oppositeElement.getResourceDescriptor(), oppositeElement);
			try {				
				// if owner: then don't do anything as we cannot change the owner
				if (ds.hasPermission(gfd, fd, DriveFilePermission.owner))
				{
					throw new SurrogateException(MessageFormat.format("Cannot rewire link from User {0} <{1}> as Owner cannot be changed through Drive API", oppositeElement.getId(), fd.getEmail()), SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC);
				}
			} catch (IOException e) {
				logger.info(MessageFormat.format("Error checking permissions user {1} on file {2} ", fd.getEmail(), gfd.getFileId()), e);
			}
			
		switch(localAction.getId())
		{
		case Collab.OACTION_File_edit:			
			addLink(fd, DriveFilePermission.writer);
			break;
		case Collab.OACTION_File_read:	
			addLink(fd, DriveFilePermission.reader);
			break;
		case Collab.OACTION_File_comment:	
			addLink(fd, DriveFilePermission.commenter);
			break;
		default:
			throw new SurrogateException(MessageFormat.format("Wiring links from Action type {0} not supported by GDriveFileSurrogate", localAction.getId()), SurrogateErrorType.UNSUPPORTED_ACTION);
		}
		status = ESurrogateStatus.REWIRING_SUCCESS;
		subject.onNext(new SurrogateEvent(thisRef, status, link));
		
		} catch (SurrogateException e) {
			status = ESurrogateStatus.REWIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, e, link));			
		} catch (HADLException e) {			
			status = ESurrogateStatus.REWIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(e.getMessage(), e, SurrogateErrorType.HADL_RETHROW), link));
		}
		subject.onCompleted();
	}

	@Override
	public void asyncDisconnectFrom(TAction localAction,
			TOperationalComponent oppositeElement,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {		
		try {
			//SORT OF PRECONDITION
			if (mode.compareTo(SurrogateMode.BRITTLE) < 0) // not able to effect permission changes
			{	
				throw new SurrogateException(MessageFormat.format("Surrogate has insufficient permissions to execute rewiring on File {0}", gfd.getFileId()), SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC);
				//ALTERNATIVE in the future would be to contact owner and ask for permissions, or to effect permissions
			}
						
			//assuming opposite user exists due to linkage connector logic and has all required info (i.e., email address)
			TGoogleUserDescriptor fd = GoogleUtils.extractUserResourceDescriptor(oppositeElement.getResourceDescriptor(), oppositeElement);
			try {				
				// if owner: then don't do anything as we cannot change the owner
				if (ds.hasPermission(gfd, fd, DriveFilePermission.owner))
				{
					throw new SurrogateException(MessageFormat.format("Cannot rewire link from User {0} <{1}> as Owner cannot be changed through Drive API", oppositeElement.getId(), fd.getEmail()), SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC);
				}
			} catch (IOException e) {
				logger.info(MessageFormat.format("Error checking permissions user {1} on file {2} ", fd.getEmail(), gfd.getFileId()), e);
			}
			
		switch(localAction.getId())
		{
		case Collab.OACTION_File_edit:			
			removeLink(fd, DriveFilePermission.writer);
			break;
		case Collab.OACTION_File_read:	
			removeLink(fd, DriveFilePermission.reader);
			break;
		case Collab.OACTION_File_comment:	
			removeLink(fd, DriveFilePermission.commenter);
			break;
		default:
			throw new SurrogateException(MessageFormat.format("Wiring links from Action type {0} not supported by GDriveFileSurrogate", localAction.getId()), SurrogateErrorType.UNSUPPORTED_ACTION);
		}
		status = ESurrogateStatus.REWIRING_SUCCESS;
		subject.onNext(new SurrogateEvent(thisRef, status, link));
		
		} catch (SurrogateException e) {
			status = ESurrogateStatus.REWIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, e, link));			
		} catch (HADLException e) {			
			status = ESurrogateStatus.REWIRING_FAILED;
			subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(e.getMessage(), e, SurrogateErrorType.HADL_RETHROW), link));
		}
		subject.onCompleted();
	}

	@Override
	public void asyncDisconnectFrom(TAction localAction,
			TOperationalConnector oppositeElement,
			BehaviorSubject<SurrogateEvent> subject, TOperationalCollabLink link) {
		// none known
		status = ESurrogateStatus.REWIRING_FAILED;
		link.setState(OperationalStateTransitionControl.doTransition(link.getState(), TOperationalState.PRESCRIBED_NONEXISTING_DENIED));
		subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException(MessageFormat.format("Unwiring links to CollaborationConnector type {0} with id {1} unknown in hADLmodel", oppositeElement.getInstanceOf().getHref(), oppositeElement.getId()), SurrogateErrorType.NOT_POSSIBLE), link));
		subject.onCompleted();
	}

	@Override
	public void asyncRelating(TOperationalObject oppositeElement,
			TObjectRef relationType, boolean isRelationOrigin,
			boolean doRemove, BehaviorSubject<SurrogateEvent> subject,
			TOperationalObjectRef ref) {
		status = ESurrogateStatus.RELATING_FAILED;
		subject.onNext(new SurrogateEvent(thisRef, status, new SurrogateException("No such relation known", SurrogateErrorType.UNSPECIFIED), ref));
		subject.onCompleted();
	}
	
	private void addLink(TGoogleUserDescriptor user, DriveFilePermission perm)
	{
		synchronized(cs)
		{
			if (cs.toRemove.containsKey(user))
			{
				if (cs.toRemove.get(user).contains(perm)) // previous request to remove
				{
					cs.toRemove.get(user).remove(perm); // DONE, no need to add to add set				
					return;
				}
			}
			// not in remove set
			if (cs.toAdd.containsKey(user))
			{
				if (cs.toAdd.get(user).contains(perm)) // already there, duplicate request, ignoring
					return;
				else
				{
					cs.toAdd.get(user).add(perm); // added to list to add, DONE
					return;
				}
			}
			else // not in set at all yet
			{
				ArrayList<DriveFilePermission> perms = new ArrayList<DriveFilePermission>();
				perms.add(perm);
				cs.toAdd.put(user, perms);
			}
		}							
	}
	
	private void removeLink(TGoogleUserDescriptor user, DriveFilePermission perm)
	{
		synchronized(cs)
		{
			if (cs.toAdd.containsKey(user))
			{
				if (cs.toAdd.get(user).contains(perm)) // previous request to add
				{
					cs.toAdd.get(user).remove(perm); // DONE, no need to add to add set				
					return;
				}
			}
			// not in add set
			if (cs.toRemove.containsKey(user))
			{
				if (cs.toRemove.get(user).contains(perm)) // already there, duplicate request, ignoring
					return;
				else
				{
					cs.toRemove.get(user).add(perm); // added to list to remove, DONE
					return;
				}
			}
			else // not in set at all yet
			{
				ArrayList<DriveFilePermission> perms = new ArrayList<DriveFilePermission>();
				perms.add(perm);
				cs.toRemove.put(user, perms);
			}
		}		
	}
	
	private boolean checkCapabilities() throws SurrogateException, IOException
	{
		// check if File exists	
		if (ds.fileExists(gfd))
		{
			File f = ds.getFileHandle(gfd);			
			if (ds.isOwner(f))
			{
				mode = SurrogateMode.SAVE;
			}
			else if (f.getWritersCanShare())
			{
				mode = SurrogateMode.BRITTLE;
			}
			else if (f.getEditable())
			{
				mode = SurrogateMode.DEGRADED;
			}
			else
			{
				mode = SurrogateMode.READONLY;
			}
			return true;
		}
		else
		{
			mode = SurrogateMode.DEFUNCT;
			throw new SurrogateException(MessageFormat.format("File with Id {0} not found or accessible", gfd.getFileId()), SurrogateErrorType.NOT_POSSIBLE);
		}
	}
	
	private class ChangeSet
	{
		Hashtable<TGoogleUserDescriptor,List<DriveFilePermission>> toAdd = new Hashtable<TGoogleUserDescriptor,List<DriveFilePermission>>();
		Hashtable<TGoogleUserDescriptor,List<DriveFilePermission>> toRemove = new Hashtable<TGoogleUserDescriptor,List<DriveFilePermission>>();	
	}		
	
	private enum SurrogateMode
	{
		SAVE,
		BRITTLE,
		DEGRADED,
		READONLY,
		DEFUNCT;
		// save mode: owner
		// brittle mode: editor with share rights
		// degraded mode: editor without share rights
		// read mode: reader or commenter
	}
}
