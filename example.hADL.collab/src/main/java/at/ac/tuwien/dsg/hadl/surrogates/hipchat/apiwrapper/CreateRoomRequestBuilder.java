package at.ac.tuwien.dsg.hadl.surrogates.hipchat.apiwrapper;

import io.evanwong.oss.hipchat.v2.commons.RequestBuilder;
import io.evanwong.oss.hipchat.v2.rooms.Privacy;

import org.apache.http.client.HttpClient;

import java.util.concurrent.ExecutorService;

/*
The MIT License (MIT)

Copyright (c) 2014 Evan Wong

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// UPDATED to use CreateRoomRequest
public class CreateRoomRequestBuilder extends RequestBuilder<CreateRoomRequest> {
    private String topic;
    private Boolean guestAcccess;
    private String name;
    //The id, email address, or mention name (beginning with an '@') of the room's owner. Defaults to the current user
    private String ownerUserId;
    private Privacy privacy;

    public CreateRoomRequestBuilder(String name, String accessToken, HttpClient httpClient, ExecutorService executorService) {
        super(accessToken, httpClient, executorService);
        this.name = name;
    }

    public CreateRoomRequestBuilder setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public CreateRoomRequestBuilder setGuestAcccess(Boolean guestAcccess) {
        this.guestAcccess = guestAcccess;
        return this;
    }

    public CreateRoomRequestBuilder setOwnerUserId(String ownerUserId) {
        this.ownerUserId = ownerUserId;
        return this;
    }

    public CreateRoomRequestBuilder setPrivacy(Privacy privacy) {
        this.privacy = privacy;
        return this;
    }

    @Override
    public CreateRoomRequest build() {
        if (name == null) {
            throw new IllegalArgumentException("name is required.");
        }
        return new CreateRoomRequest(topic, guestAcccess, name, ownerUserId, privacy, accessToken, httpClient, executorService);
    }
}
