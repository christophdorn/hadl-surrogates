# README #

This README briefly outlines the step needed to run the example hADL execution framework usecase.

### What is this repository for? ###

* This repository provides surrogates for Google Drive and HipChat
* Initial purpose is hosting the evaluation use case and implementation for the CAiSE'2016 submission.

### How do I get set up? ###

* Configuration
     * Create a HipChat group
     * Obtain a HipChat API v2 access token from the group admin user account (not admin but the user) with the following scopes: Administer Room, Manage Rooms, Send Notification, View Group, View Room.
     * Create a Project on the Google Developer Console with access to the DriveAPI and obtain/download a .p12 key and note down the service account email address.
     * Setup/Collect 5 google users (gmail addresses) for GoogleDrive
     * Use the same user details for setting up 5 HipChat users
     * Add the 5 HipChat users to the newly created HipChat group
     * Add all these details to your java class implementing the IUseCaseConfig interface (The TGoogleUserDescriptors need to have the user's emailaddress set for 'Email' and 'Id')


* Dependencies: 
     * hADL execution framework on Bitbucket: https://bitbucket.org/christophdorn/hadl (neo4jbackend branch)
     * Other dependencies: Java7, JUnit4, otherwise managed via MAVEN (pom included)
* Database configuration: none, hADL execution framework uses in-memory Neo4J for demonstration purpose
* How to run tests: 
    * the two available test just implement the use case, to be found in UseCaseTester.java (usual JUnit location)
    * UseCaseUtils demonstrates the setup of the hADL execution framework (incl. where to put credentials etc)
    * UseCaseUtils also acts as a wrapper around hADL execution framework interface
    * Insert and provide your demo users and access credential details in a class implementing the IUseCaseConfig interface
* Deployment: just run on your local machine, Internet access to contact Google Drive API and HipChat API required

### Contribution guidelines ###

* Get in contact with the repo owner in case you like to contribute

### Who do I talk to? ###

* Repo owner